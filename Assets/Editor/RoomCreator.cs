﻿using UnityEngine;
using UnityEditor;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.0 
// ------------------------------------------------------------------
// Description:
// An editor window that allows a user to quickly generate a basic 
// room using a floor object and a wall object.
// ------------------------------------------------------------------
// Updates:
//
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

public class RoomCreator : EditorWindow
{
    // Private Variables ------------------------------------------------------
    private int _roomWidth;
    private int _roomHeight;

    private float _tileSize;

    private Vector3 _startPosition;

    private GameObject _floorObject;
    private GameObject _wallObject;
    private GameObject _roomRoot;

    // Static Functions -------------------------------------------------------

    [MenuItem( "Tools/Create Room" )]
    private static void InitWindow()
    {
        RoomCreator window = EditorWindow.GetWindow<RoomCreator>( true, "Room Creator", true );
        window.minSize = new Vector2( 350.0f, 250.0f );
        window.maxSize = new Vector2( 350.0f, 250.0f );
        window.Show();
    }

    // Unity Functions --------------------------------------------------------

    void OnGUI()
    {
        _floorObject = EditorGUILayout.ObjectField( "Floor : ", _floorObject, typeof( GameObject ), false ) as GameObject;
        _wallObject = EditorGUILayout.ObjectField( "Wall : ", _wallObject, typeof( GameObject ), false ) as GameObject;

        _startPosition = EditorGUILayout.Vector3Field( "Start Point", _startPosition );

        EditorGUILayout.PrefixLabel( "Tile size" );
        _tileSize = EditorGUILayout.FloatField( _tileSize );
        EditorGUILayout.PrefixLabel( "Room width" );
        _roomWidth = EditorGUILayout.IntField( _roomWidth );
        EditorGUILayout.PrefixLabel( "Room height" );
        _roomHeight = EditorGUILayout.IntField( _roomHeight );

        if( GUILayout.Button( "Generate Room" ) )
        {
            CreateRoom();
        }

        if( GUILayout.Button( "New Room" ) )
        {
            _roomRoot = null;
        }
    }

    // Private Functions ------------------------------------------------------

    //Generates a room based on user paramaters
    private void CreateRoom()
    {
        if( _roomRoot != null )
        {
            DestroyImmediate( _roomRoot );
        }
        _roomRoot = new GameObject( "Room" );

        _roomRoot.transform.position = _startPosition;

        CreateFloor();
        CreateWalls();
    }

    //Generates the floor of a room
    private void CreateFloor()
    {
        for( int x = 0; x < _roomWidth; ++x )
        {
            for( int z = 0; z < _roomHeight; ++z )
            {
                CreateRoomAsset( _floorObject, x * _tileSize, z * _tileSize, Quaternion.identity );
            }
        }
    }

    //Generates the walls of a room
    private void CreateWalls()
    {
        for( int x = 0; x < _roomWidth; ++x )
        {
            float zPosition = -_tileSize * 0.5f;
            CreateRoomAsset( _wallObject, x * _tileSize, zPosition, Quaternion.identity );

            zPosition = (_tileSize * 0.5f) + (_roomHeight - 1) * _tileSize;
            CreateRoomAsset( _wallObject, x * _tileSize, zPosition, Quaternion.identity );
        }

        for( int z = 0; z < _roomHeight; ++z )
        {
            float xPosition = -_tileSize * 0.5f;
            CreateRoomAsset( _wallObject, xPosition, z * _tileSize, Quaternion.AngleAxis( 90, Vector3.up ) );

            xPosition = (_tileSize * 0.5f) + (_roomWidth - 1) * _tileSize;
            CreateRoomAsset( _wallObject, xPosition, z * _tileSize, Quaternion.AngleAxis( 90, Vector3.up ) );
        }
    }

    //Places a room asset in relation to the startPosition
    private void CreateRoomAsset( GameObject asset, float xOffset, float zOffset, Quaternion rotation )
    {
        Vector3 position = _startPosition;
        position.x += xOffset;
        position.z += zOffset;

        GameObject floor = PrefabUtility.InstantiatePrefab( asset ) as GameObject;
        floor.transform.position = position;
        floor.transform.rotation = rotation;

        floor.transform.SetParent( _roomRoot.transform );
        floor.name = floor.name.Replace( "(Clone)", "" );
    }
}
