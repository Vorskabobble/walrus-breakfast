﻿using UnityEngine;
using UnityEditor;

public class ItemSpawnerHelper
{
    [MenuItem("Tools/Create Item Spawner")]
    private static void CreateItemSpawner()
    {
        GameObject itemSpawner = new GameObject();
        itemSpawner.name = Selection.activeObject.name + " Spawner";

        ItemSpawner component = itemSpawner.AddComponent<ItemSpawner>();
        component.Spawnable = Selection.activeObject as ItemData;
        
    }

    [MenuItem("Tools/Create Item Spawner", true)]
    private static bool ValidateItemToSpawn()
    {
        if( Selection.activeObject != null )
        {
            return Selection.activeObject.GetType() == typeof( ItemData );
        }
        return false;
    }
}