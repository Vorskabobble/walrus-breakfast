﻿using UnityEngine;
using UnityEditor;

public class CreatePrefabContext
{
    [MenuItem("GameObject/Create Prefab %'")]
    private static void CreatePrefab()
    {
        GameObject newPrefab = PrefabUtility.CreatePrefab("Assets/Prefabs/" + Selection.activeGameObject.name + ".prefab", Selection.activeGameObject, ReplacePrefabOptions.ConnectToPrefab);
        EditorUtility.SetDirty(newPrefab);
    }

    [MenuItem("GameObject/Create Prefab %'", true)]
    private static bool ValidateSeleciton()
    {
        return Selection.activeGameObject != null;
    }
}