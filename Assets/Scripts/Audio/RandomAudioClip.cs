﻿using UnityEngine;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.0 
// ------------------------------------------------------------------
// Description:
// Changes the "AudioClip" of an "AudioSource" to a random 
// "AudioClip" from a list of "AudioClip"s
// ------------------------------------------------------------------
// Updates:
//
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

[RequireComponent( typeof( AudioSource ) )]
public class RandomAudioClip : MonoBehaviour
{
    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private AudioClip[] _audioClips;

    // Private Variables ------------------------------------------------------
    private AudioSource _audioSource;

    // Unity Functions --------------------------------------------------------

    void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    void Start()
    {
        ChangeClip();
    }

    // Public Functions -------------------------------------------------------

    //Changes current clip to another random one
    public void ChangeClip()
    {
        _audioSource.clip = _audioClips[Random.Range( 0, _audioClips.Length )];
    }

    public void PlayRandom()
    {
        ChangeClip();
        _audioSource.Play();
    }
}
