﻿using UnityEngine;

public abstract class BaseUnlock : ScriptableObject, IUnlock
{
    public abstract bool IsUnlocked();
}
