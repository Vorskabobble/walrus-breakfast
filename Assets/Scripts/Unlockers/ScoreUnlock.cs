﻿using UnityEngine;

[CreateAssetMenu(fileName ="New Score Unlock", menuName ="Unlock/Score Unlock")]
public class ScoreUnlock : BaseUnlock
{
    [SerializeField]
    private float _scoreRequirement;

    [SerializeField]
    private ScoreData _scoreData;

    public override bool IsUnlocked()
    {
        if(_scoreRequirement >= _scoreData._score)
        {
            return true;
        }
        return false;
    }
}
