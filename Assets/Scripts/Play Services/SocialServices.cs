﻿using GooglePlayGames;

using UnityEngine;
using UnityEngine.Events;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.1 
// ------------------------------------------------------------------
// Description:
// Handles the social aspect of the game, logging in, updating
// leaderboards and achievements.
// ------------------------------------------------------------------
// Updates:
// 1.1 + Added achievement functions
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

public class SocialServices : MonoBehaviour
{
    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private UnityEvent _userAuthenticated;
    [SerializeField]
    private UnityEvent _newPersonalHigh;
    [SerializeField]
    private UnityEvent _newPerfectScore;
    [SerializeField]
    private UnityEvent _newItemCountScore;

    // Unity Functions --------------------------------------------------------

    void Start()
    {
        PlayGamesPlatform.Activate();
    }

    // Public Functions -------------------------------------------------------

    //Authenticates with google play game services
    public void SignIn()
    {
        Social.localUser.Authenticate( ( bool success ) =>
         {
             if( success )
             {
                 _userAuthenticated.Invoke();
             }
         } );
    }

    //Add new scores to the leaderboards, discards if not the players highest
    public void UpdateLeaderboard( ScoreData score )
    {
        if( Social.localUser.authenticated )
        {
            Social.ReportScore( (long)score._score, GPGSIds.leaderboard_high_scores, ( bool success ) =>
             {
                 if( success )
                 {
                     _newPersonalHigh.Invoke();
                 }
             } );

            Social.ReportScore( score._perfectScores, GPGSIds.leaderboard_perfect_scores, ( bool success ) =>
           {
               if( success )
               {
                   _newPerfectScore.Invoke();
               }
           } );

            Social.ReportScore( score._collectedItems, GPGSIds.leaderboard_collected_items, ( bool success ) =>
            {
                _newItemCountScore.Invoke();
            } );
        }
    }

    //Unlock an achievement
    public void UnlockAchievement( string id )
    {
        Social.ReportProgress( id, 100.0, ( bool success ) => { } );
    }

    //Update an incremental achievement by value
    public void UpdateAchievement( string id, int value )
    {
        PlayGamesPlatform.Instance.IncrementAchievement( id, value, ( bool success ) => { } );
    }

    //Displays the highscore leaderboard
    public void ShowHighScoreLeaderboard()
    {
        ((PlayGamesPlatform)Social.Active).ShowLeaderboardUI( GPGSIds.leaderboard_high_scores );
    }

    public void ShowLeaderboards()
    {
        ((PlayGamesPlatform)Social.Active).ShowLeaderboardUI();
    }

    //Displays the achievements UI
    public void ShowAchievements()
    {
        ((PlayGamesPlatform)Social.Active).ShowAchievementsUI();
    }
}
