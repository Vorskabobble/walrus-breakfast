﻿using UnityEngine;
using UnityEngine.Events;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.0 
// ------------------------------------------------------------------
// Description:
// Checks if areas meet their requirements and unlocks them
// ------------------------------------------------------------------
// Updates:
//
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

public class AreaUnlocker : MonoBehaviour
{
    // Static Readonlys -------------------------------------------------------
    private static readonly string OPEN_PARAM_NAME = "Open";

    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private AreaUnlockData[] _areas;
    [SerializeField]
    private CollectionPoint _collectionPoint;
    [SerializeField]
    private UnityEvent _areaUnlockedEvent;

    // Private Variables ------------------------------------------------------
    private int _openParamID;

    // Unity Functions --------------------------------------------------------

    void Start()
    {
        _openParamID = Animator.StringToHash( OPEN_PARAM_NAME );
    }

    // Public Functions -------------------------------------------------------

    //Will unlock any areas that meet their requirements
    public void UnlockAreas()
    {
        foreach( AreaUnlockData area in _areas )
        {
            if( area.Unlock.IsUnlocked() )
            {
                GameObject doorsToOpen = GameObject.Find( area._doorsToOpenID );
                GameObject fogToFade = GameObject.Find( area._fogToFadeID );

                if( doorsToOpen != null )
                {
                    Animator animator = doorsToOpen.GetComponent<Animator>();
                    if( animator != null )
                    { animator.SetBool( _openParamID, true ); }
                }

                if( fogToFade != null )
                {
                    ObjectFade fadeable = fogToFade.GetComponent<ObjectFade>();
                    if( fadeable != null )
                    { fadeable.PermenentFade(); }
                }

                if( area._unlockedItems.Length > 0 && _collectionPoint != null )
                {
                    _collectionPoint.AddAcceptedItems( area._unlockedItems );
                }

                _areaUnlockedEvent.Invoke();
            }
        }
    }
}
