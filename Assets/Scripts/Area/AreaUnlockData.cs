﻿using UnityEngine;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.0 
// ------------------------------------------------------------------
// Description:
// Data storage for requirements and unlocks of different areas
// ------------------------------------------------------------------
// Updates:
//
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

[CreateAssetMenu(fileName ="New Area")]
public class AreaUnlockData : ScriptableObject
{
    // Public Variables -------------------------------------------------------
    [Header("Requirements")]
    public BaseUnlock Unlock;

    [Header("Unlocks")]
    public string _doorsToOpenID;
    public string _fogToFadeID;
    public ItemData[] _unlockedItems;
}
