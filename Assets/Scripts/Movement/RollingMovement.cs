﻿using UnityEngine;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.0 
// ------------------------------------------------------------------
// Description:
// Attaching this to a game object lets it roll around
// like a ball.
// ------------------------------------------------------------------
// Updates:
//
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

[RequireComponent(typeof(Rigidbody))]
public class RollingMovement : MonoBehaviour, IMovement
{
    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private float _maxTorque;

    // Private Variables ------------------------------------------------------
    private Vector3 _curTorque;

    private Rigidbody _rigidbody;

    // Interface implementation -----------------------------------------------

    void IMovement.Move(Vector3 input)
    {
        _curTorque = Vector3.forward * -input.x * _maxTorque;
        _curTorque += Vector3.right * input.z * _maxTorque;
    }

    void IMovement.Turn(Vector3 input)
    {
        Debug.LogWarning("IMovement.Turn(Vector3 input) not implemented in RollingMovement");
    }
   
    // Unity Functions --------------------------------------------------------

    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>() as Rigidbody;
    }
        
    void FixedUpdate()
    {
        _rigidbody.AddTorque(_curTorque * Time.fixedDeltaTime);
    }
}
