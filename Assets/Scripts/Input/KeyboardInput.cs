﻿using UnityEngine;
using System.Collections;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.0 
// ------------------------------------------------------------------
// Description:
// Uses input from the keyboard and passes that to the 
// player.
// ------------------------------------------------------------------
// Updates:
//
// ------------------------------------------------------------------
// TODO:
// + Create an IInput interface and inherit
// + Return ICommand instead of calling functions directly
// ============================================================================

public class KeyboardInput : MonoBehaviour
{
    // Private Variables ------------------------------------------------------
    private IMovement _movement;

    // Unity Functions --------------------------------------------------------

	void Start ()
    {
        _movement = GetComponent(typeof(IMovement)) as IMovement;
	}
	
    void FixedUpdate()
    {
        float yMove = Input.GetAxis( "Vertical" );
        float xMove = Input.GetAxis( "Horizontal" );

        if ( _movement != null )
        {
            _movement.Move(new Vector3(xMove, 0.0f, yMove));
        }
    }
}
