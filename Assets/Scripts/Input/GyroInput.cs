﻿using UnityEngine;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.2
// ------------------------------------------------------------------
// Description:
// Initialises and provides input form the gyroscope
// ------------------------------------------------------------------
// Updates:
// 1.2 + Now uses gyro.gravity and calculates dot product
// 1.2 - No longer uses gyro.attitude
// 1.2 - Removed CalibrateGyro() now gets manually set 
// 1.2 ~ Replaced Enable/Disable gyro functions with property
// ..................................................................
// 1.1 ~ now uses "Mathf.DeltaAngle()" for calculating angle
// ------------------------------------------------------------------
// TODO:
// Change to make use of dot product of gravity and v3.up or v3.right
// ============================================================================

public class GyroInput : MonoBehaviour
{
    // Constants --------------------------------------------------------------
    private const float ANGLE_TO_DOT_RANGE = 0.0055555556f;

    // Editor Variables -------------------------------------------------------
    [SerializeField]
    float _maxRotationDeg;
    [SerializeField]
    Vector2 _zeroAngleAsDot;

    // Private Variables ------------------------------------------------------
    bool _gyroInputEnabled;
    float _angleToInput;
    float _angleAsDotRange;

    IMovement _movement;

    // Properties -------------------------------------------------------------
    public bool InputEnabled
    { get { return _gyroInputEnabled; } set { _gyroInputEnabled = value; } }

    // Unity Functions --------------------------------------------------------

    void Start()
    {
        _movement = GetComponent( typeof( IMovement ) ) as IMovement;

        _angleAsDotRange = _maxRotationDeg * ANGLE_TO_DOT_RANGE;
        _angleToInput = 2.0f / (_angleAsDotRange * 2);

        InputEnabled = false;
    }

    void FixedUpdate()
    {
        if( _gyroInputEnabled )
        {
            Vector3 gravity = Input.gyro.gravity;

            float forwardDot = Vector3.Dot( gravity, Vector3.up );
            float sidewaysDot = Vector3.Dot( gravity, Vector3.right );

            //Get difference between zero and device
            float forwardAngle = forwardDot - _zeroAngleAsDot.x;
            float sidewaysAngle = sidewaysDot - _zeroAngleAsDot.y;

            //Keep angle within rotation range
            forwardAngle = Mathf.Clamp( forwardAngle, -_angleAsDotRange, _angleAsDotRange );
            sidewaysAngle = Mathf.Clamp( sidewaysAngle, -_angleAsDotRange, _angleAsDotRange );

            Vector3 moveInput = new Vector3( sidewaysAngle * _angleToInput, 0.0f, forwardAngle * _angleToInput );
            _movement.Move( moveInput );
        }
    }
}
