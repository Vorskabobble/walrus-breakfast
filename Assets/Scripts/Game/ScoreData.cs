﻿using UnityEngine;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.0 
// ------------------------------------------------------------------
// Description:
// 
// ------------------------------------------------------------------
// Updates:
//
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

[CreateAssetMenu(fileName ="Player Score")]
public class ScoreData : ScriptableObject
{
    // Public Variables -------------------------------------------------------
    public bool _hasPerfectScore;
    public bool _timeBoostedRound;

    public int _perfectScores;
    public int _collectedItems;

    public float _score;
    public float _perfectMultiplier;

    // Properties -------------------------------------------------------------
    //Property so variable can be set using Unity Events in the editor
    public bool TimeBoostedRound
    { get { return _timeBoostedRound; } set { _timeBoostedRound = value; } }
}