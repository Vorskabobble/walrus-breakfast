﻿using UnityEngine;

using GooglePlayGames;
using UnityEngine.SocialPlatforms;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.0
// ------------------------------------------------------------------
// Description:
// Performs game initialisation one the splash screen and loads game
// level
// ------------------------------------------------------------------
// Updates:
// 
// ------------------------------------------------------------------
// TODO:
// 
// ============================================================================

public class GameInit : MonoBehaviour
{
    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private float _secondsBeforeLoad;
    [SerializeField]
    private GameLifetime _gameService;

    // Private Variables ------------------------------------------------------
    private float _sceneTime = 0.0f;

    // Unity Functions --------------------------------------------------------

    void Start()
    {
        _gameService.EnableGyro();
    }

    void Update()
    {
        if( _sceneTime > _secondsBeforeLoad )
        {
			if (PlayerPrefs.GetInt ("firstTime", 0) == 0) 
			{
				_gameService.LoadScene (1);
				PlayerPrefs.SetInt("firstTime", 1);
			} 
			else
			{
				_gameService.LoadScene(2);
			}
        }
        _sceneTime += Time.deltaTime;
    }
}
