﻿using UnityEngine;
using UnityEngine.Events;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.0 
// ------------------------------------------------------------------
// Description:
// Calls an event at random times.
// ------------------------------------------------------------------
// Updates:
//
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

public class RandomTimeEvent : MonoBehaviour
{
    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private float _minTime;
    [SerializeField]
    private float _maxTime;

    [SerializeField]
    private UnityEvent _event;

    // Private Variables ------------------------------------------------------
    private float _curTime = 0.0f;
    private float _timeToWait = 0.0f;

    // Unity Functions --------------------------------------------------------

    void Start()
    {
        _timeToWait = Random.Range( _minTime, _maxTime );
    }

    void Update()
    {
        _curTime += Time.deltaTime;
        if( _curTime >= _timeToWait )
        {
            CallEvent();
        }
    }

    // Private Variables ------------------------------------------------------

    private void CallEvent()
    {
        _event.Invoke();
        _curTime = 0.0f;
        _timeToWait = Random.Range( _minTime, _maxTime );
    }
}
