﻿using UnityEngine;
using UnityEngine.SceneManagement;
// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.0 
// ------------------------------------------------------------------
// Description:
// Provides functions that can be called through UnityEvent.
// ------------------------------------------------------------------
// Updates:
//
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

public class GameLifetime : MonoBehaviour 
{
	// Public Functions -------------------------------------------------------

    //Load a scene
    public void LoadScene(int sceneID)
    {
        SceneManager.LoadScene( sceneID );
    }

    public void EnableGyro()
    {
        Input.gyro.enabled = true;
    }
}
