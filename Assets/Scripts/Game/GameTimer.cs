﻿using System.Collections;

using UnityEngine;
using UnityEngine.Events;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.1
// ------------------------------------------------------------------
// Description:
// This acts as a timer that counts down to zero. Time can also be
// added (not instance, happens over "_timeToAddTime"
// ------------------------------------------------------------------
// Updates:
// 1.1 ~ Stopped timer falling below zero
// ------------------------------------------------------------------
// TODO:
// 
// ============================================================================

public class GameTimer : MonoBehaviour
{
    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private float _roundTime;
    [SerializeField]
    private float _timeToAddTime;
    [SerializeField]
    private UnityEvent _endGameEvent;

    // Private Variables ------------------------------------------------------
    private bool _timerRunning = false;
    
    // Properties -------------------------------------------------------------
    public float RoundTimer
    { get { return _roundTime; } }

    // Unity Functions --------------------------------------------------------

    void Update()
    {
        if (_timerRunning)
        {
            _roundTime -= Time.deltaTime;

            if (_roundTime <= 0.0f)
            {
                _roundTime = 0.0f;
                _timerRunning = false;
                _endGameEvent.Invoke();
            }

        }
    }

    // Public Functions -------------------------------------------------------

    //Sdd time to the timer, adds over time
    public void AddTime(float seconds)
    {
        StartCoroutine(AddTimeOverTime(seconds));
    }

    //Starts the time
    public void StartTimer()
    {
        _timerRunning = true;
    }

    //pauses the timer
    public void PauseTimer()
    {
        _timerRunning = false;
    }

    // Private Functions ------------------------------------------------------

    //Increases "_roundTimer" by "timeToAdd" over "_timeToAddTime" seconds
    private IEnumerator AddTimeOverTime(float timeToAdd)
    {
        float curTime = 0.0f;
        float timePerSecond = timeToAdd / _timeToAddTime;
        while (curTime <= _timeToAddTime)
        {
            _roundTime += timePerSecond * Time.deltaTime;
            curTime += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }
    }
}
