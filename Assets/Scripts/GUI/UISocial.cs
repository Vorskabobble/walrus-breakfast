﻿using UnityEngine;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.1
// ------------------------------------------------------------------
// Description:
// Handles updating Social UI and providing any callbacks for UI 
// elements
// ------------------------------------------------------------------
// Updates:
// 1.1 + Enables social buttons automatically if authenticated
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

public class UISocial : MonoBehaviour
{
    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private GUISwitcher _guiSwitcher;

    [SerializeField]
    private Animator _signInButton;
    [SerializeField]
    private Animator[] _socialButtons;

    // Unity Functions --------------------------------------------------------

    public void OnEnable()
    {
        if( Social.localUser.authenticated )
        {
            EnableSocialButtons();
        }
        else
        {
            DisableSocialButtons();
        }

    }

    // Public Functions -------------------------------------------------------

    public void EnableSocialButtons()
    {
        _guiSwitcher.HideElement( _signInButton );

        foreach( Animator button in _socialButtons )
        {
            _guiSwitcher.ShowElement( button );
        }
    }

    public void DisableSocialButtons()
    {
        _guiSwitcher.ShowElement( _signInButton );

        foreach( Animator button in _socialButtons )
        {
            _guiSwitcher.HideElement( button );
        }
    }
}
