﻿using UnityEngine;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.0 
// ------------------------------------------------------------------
// Description:
// Using in conjunction with the GUIElementController to set the
// starting state;
// ------------------------------------------------------------------
// Updates:
//
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

public class GUIStartState : MonoBehaviour 
{
    // Static Readonlys -------------------------------------------------------
    private static readonly string SHOW_PARAM_NAME = "Show";
    private static readonly string SHOW_STATE_NAME = "ShowGUIElement";
    private static readonly string HIDE_STATE_NAME = "HideGUIElement";

    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private bool _startShown;

    // Private Functions ------------------------------------------------------
    private int _showBoolID;
    private int _showStateID;
    private int _hideStateID;

    private Animator _animator;

    // Unity Functions --------------------------------------------------------

    void Awake()
    {
        _animator = GetComponent<Animator>();
        _showBoolID = Animator.StringToHash( SHOW_PARAM_NAME );
        _showStateID = Animator.StringToHash( SHOW_STATE_NAME );
        _hideStateID = Animator.StringToHash( HIDE_STATE_NAME );
    }

    void OnEnable()
    {
        if(_animator != null)
        {
            _animator.SetBool( _showBoolID, _startShown );
            if( _startShown )
            {
                _animator.Play( _showStateID );
            }
            else
            {
                _animator.Play( _hideStateID );
            }
        }
        Destroy( this );
    }
}
