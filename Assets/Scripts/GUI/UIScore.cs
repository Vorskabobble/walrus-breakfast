﻿using UnityEngine;
using UnityEngine.UI;
// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.0
// ------------------------------------------------------------------
// Description:
// Handles updating Score UI and providing any callbacks for UI 
// elements
// ------------------------------------------------------------------
// Updates:
//
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

public class UIScore : MonoBehaviour
{
    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private GUISwitcher _guiSwitcher;
    [SerializeField]
    private Animator _extraTimeButton;

    [Header( "Score" )]
    [SerializeField]
    private ScoreData _score;
    [SerializeField]
    private Text _scoreText;
    [SerializeField]
    private Text _perfectScoresText;
    [SerializeField]
    private Text _collectedItemsText;

    // Unity Functions --------------------------------------------------------

    void OnEnable()
    {
        UpdateScoreDisplay();

        if (UnityEngine.Advertisements.Advertisement.IsReady("rewardedVideo") && !_score._timeBoostedRound)
        {
            EnableRewardButton();
        }
    }

    // Public Functions -------------------------------------------------------

    public void EnableRewardButton()
    {
        _guiSwitcher.ShowElement( _extraTimeButton );
    }

    public void UpdateScoreDisplay()
    {
        _scoreText.text = _score._score.ToString();
        _perfectScoresText.text = _score._perfectScores.ToString();
        _collectedItemsText.text = _score._collectedItems.ToString();
    }
}
