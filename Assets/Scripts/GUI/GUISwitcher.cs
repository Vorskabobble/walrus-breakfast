﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

using System.Collections;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.3
// ------------------------------------------------------------------
// Description:
// This provides functions to switch between different
// GUI screens, disabling GUI's sent off screen. 
// "Open" is the animator parameter name used.
// "CloseGUI" is the animator state used.
// ------------------------------------------------------------------
// Updates:
// 1.3 - No longer automatically hides old GUI, GUI can be layered
// 1.2 + ShowElement() and HideElement() functions for buttons etc.
// 1.1 + is null check to HideGUI() for "_currentGUI"
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

public class GUISwitcher : MonoBehaviour
{
    // Static Readonlys -------------------------------------------------------
    private static readonly string OPEN_PARAM_NAME = "Open";
    private static readonly string SHOW_PARAM_NAME = "Show";
    private static readonly string CLOSED_STATE_NAME = "CloseGUI";
    private static readonly string HIDE_STATE_NAME = "HideGUIElement";

    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private Animator[] _firstGUI;

    // Private Variables ------------------------------------------------------
    private int _openBoolID;
    private int _showBoolID;

    private int _hideStateID;

    //private Animator _currentGUI;

    // Unity Functions --------------------------------------------------------

    void Awake()
    {
        _openBoolID = Animator.StringToHash( OPEN_PARAM_NAME );
        _showBoolID = Animator.StringToHash( SHOW_PARAM_NAME );

        _hideStateID = Animator.StringToHash( HIDE_STATE_NAME );
    }

    void Start()
    {
        foreach( Animator anim in _firstGUI )
        {
            ShowGUI( anim );
        }
    }

    // Public Functions -------------------------------------------------------

    //Hide the current GUI and show the one passed
    public void ShowGUI( Animator animator )
    {
        if( animator.gameObject.activeInHierarchy && animator.GetBool( _openBoolID ) )
        { return; }

        //Enable GUI and bring it to the front
        animator.gameObject.SetActive( true );
        animator.transform.SetAsLastSibling();
        animator.SetBool( _openBoolID, true );

        SetSelectedGUIElement( GetSelectableElement( animator.gameObject ) );
    }

    //Hide the current GUI
    public void HideGUI( Animator animator )
    {
        animator.SetBool( _openBoolID, false );
        StartCoroutine( DelayedGUIDisable( animator ) );
    }

    //Show a GUI element
    public void ShowElement( Animator animator )
    {
        animator.SetBool( _showBoolID, true );
    }

    //Hide a GUI element
    public void HideElement( Animator animator )
    {
        animator.SetBool( _showBoolID, false );
    }

    public void InstantElementHide( Animator animator )
    {
        animator.SetBool( _showBoolID, false );
        animator.Play( _hideStateID );
    }

    // Private Functions ------------------------------------------------------

    // Set selection to "GUIElement"
    private void SetSelectedGUIElement( GameObject GUIElement )
    {
        EventSystem.current.SetSelectedGameObject( GUIElement );
    }

    //Loop through all children and return first active and interactable selectable
    private GameObject GetSelectableElement( GameObject root )
    {
        Selectable[] selectables = root.GetComponentsInChildren<Selectable>();

        foreach( Selectable selectable in selectables )
        {
            if( selectable.IsActive() && selectable.IsInteractable() )
            {
                return selectable.gameObject;
            }
        }

        return null;
    }

    //Disables GUI after it's transitioned to closed state
    private IEnumerator DelayedGUIDisable( Animator animator )
    {
        bool inClosedState = false;
        bool shouldClose = true;

        while( !inClosedState && shouldClose )
        {
            inClosedState = animator.GetCurrentAnimatorStateInfo( 0 ).IsName( CLOSED_STATE_NAME );
            shouldClose = !animator.GetBool( _openBoolID );

            yield return new WaitForEndOfFrame();
        }

        if( shouldClose )
        {
            animator.gameObject.SetActive( false );
        }
    }
}
