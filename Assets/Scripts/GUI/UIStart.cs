﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

using System.Collections;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.0
// ------------------------------------------------------------------
// Description:
// Handles updating Start UI and providing any callbacks for UI 
// elements
// ------------------------------------------------------------------
// Updates:
//
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

public class UIStart : MonoBehaviour
{
    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private UnityEvent _startGameEvent;
    [SerializeField]
    private GUISwitcher _guiSwitcher;

    [SerializeField]
    private Animator _startButton;
    [SerializeField]
    private Animator[] _countdownImages;

    // Unity Functions --------------------------------------------------------

    void OnEnable()
    {
        SetStartState();
    }

    // Public Functions -------------------------------------------------------

    public void StartGame()
    {
        _guiSwitcher.InstantElementHide( _startButton );
        StartCoroutine( RunCountdownImages() );
    }

    // Private Functions ------------------------------------------------------

    private void SetStartState()
    {
        _guiSwitcher.ShowElement( _startButton );
        foreach( Animator image in _countdownImages )
        {
            _guiSwitcher.HideElement( image );
        }
    }

    private IEnumerator RunCountdownImages()
    {
        Animator lastAnim = null;
        for( int i = 0; i < _countdownImages.Length; ++i )
        {
            if( lastAnim != null )
            {
                _guiSwitcher.HideElement( lastAnim );
            }

            _guiSwitcher.ShowElement( _countdownImages[i] );
            lastAnim = _countdownImages[i];

            yield return new WaitForSeconds( 1 );
        }
        _guiSwitcher.HideElement( lastAnim );
        _startGameEvent.Invoke();
    }
}
