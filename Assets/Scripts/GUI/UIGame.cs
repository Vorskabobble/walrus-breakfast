﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.0
// ------------------------------------------------------------------
// Description:
// Handles updating Game UI and providing any callbacks for UI 
// elements.
// ------------------------------------------------------------------
// Updates:
//
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

public class UIGame : MonoBehaviour
{
    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private GUISwitcher _guiSwitcher;

    [Header( "Timer" )]
    [SerializeField]
    private Text _timerText;
    [SerializeField]
    private GameTimer _timer;
    [SerializeField]
    private AudioSource _runningOutSound;

    [Header( "Objective" )]
    [SerializeField]
    private Image[] _itemIcons;
    [SerializeField]
    private CollectionObjectiveTracker _objectiveTracker;

    [Header( "Score" )]
    [SerializeField]
    private ScoreData _score;
    [SerializeField]
    private float _scoreTypeDisplayTime;
    [SerializeField]
    private Animator _perfectScoreDisplay;
    [SerializeField]
    private Animator _goodScoreDisplay;
    [SerializeField]
    private AudioSource _perfectScoreSound;
    [SerializeField]
    private AudioSource _goodScoreSound;

    [Header("Invetory")]
    [SerializeField]
    private Text _inventoryFullText;
    [SerializeField]
    private Inventory _playerInventory;

    // Unity Functions --------------------------------------------------------

    void OnEnable()
    {
        UpdateObjectiveStatus();
    }

    void Update()
    {
        _timerText.text = SecondsToClock( _timer.RoundTimer );

        if( _timer.RoundTimer <= 5 && _timer.RoundTimer != 0.0f)
        {
            if(!_runningOutSound.isPlaying)
            {
                _runningOutSound.Play();
            }
        }

        CheckFullInventory();
    }

    // Public Functions -------------------------------------------------------

    //check if tick sprite should be shown on items
    public void UpdateObjectiveStatus()
    {
        bool[] itemIsChecked = new bool[_itemIcons.Length];
        foreach( ItemData item in _objectiveTracker.CollectedItems )
        {
            for( int i = 0; i < _itemIcons.Length && i < _objectiveTracker.ItemsToCollect.Length; ++i )
            {
                if( _objectiveTracker.ItemsToCollect[i] == item && !itemIsChecked[i] )
                {
                    _guiSwitcher.ShowElement( _itemIcons[i].transform.Find( "Tick" ).GetComponent<Animator>() );
                    itemIsChecked[i] = true;
                    break;
                }
            }
        }
    }

    //Make sure item sprites are the same as those required for objective
    public void UpdateObjectiveIcons()
    {
        if( _itemIcons.Length <= 0 && _objectiveTracker.ItemsToCollect.Length <= 0 )
        { return; }

        for( int i = 0; i < _itemIcons.Length && i < _objectiveTracker.ItemsToCollect.Length; ++i )
        {
            _itemIcons[i].sprite = _objectiveTracker.ItemsToCollect[i]._icon;
            _guiSwitcher.HideElement( _itemIcons[i].transform.Find( "Tick" ).GetComponent<Animator>() );
        }
    }

    //Show perfect or good when objective complete
    public void ShowScoreType()
    {
        if( _score._hasPerfectScore )
        {
            StartCoroutine( ShowElementForTime( _perfectScoreDisplay, _scoreTypeDisplayTime ) );
            _perfectScoreSound.Play();
        }
        else
        {
            StartCoroutine( ShowElementForTime( _goodScoreDisplay, _scoreTypeDisplayTime ) );
            _goodScoreSound.Play();
        }
    }
    
    // Private Functions ------------------------------------------------------

    //Convert seconds to a string in a digital clock format
    private string SecondsToClock( float seconds )
    {
        int minutes = (int)seconds / 60;
        float secs = seconds % 60.0f;

        return minutes.ToString( "00" ) + ":" + secs.ToString( "00.0" );
    }

    private IEnumerator ShowElementForTime( Animator element, float time )
    {
        _guiSwitcher.ShowElement( element );
        yield return new WaitForSeconds( time );
        _guiSwitcher.HideElement( element );
    }

    private void CheckFullInventory()
    {
        if(_playerInventory.Items.Count == _playerInventory.MaxItems)
        {
            _inventoryFullText.enabled = true;
        }
        else
        {
            _inventoryFullText.enabled = false;
        }
    }
}
