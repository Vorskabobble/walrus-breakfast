﻿public interface IUnlock
{
    bool IsUnlocked();
}
