﻿using UnityEngine;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.0 
// ------------------------------------------------------------------
// Description:
// General interface for any movement class to inherit.
// ------------------------------------------------------------------
// Updates:
// 
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

public interface IMovement
{
    void Move(Vector3 input);
    void Turn(Vector3 input);
}
