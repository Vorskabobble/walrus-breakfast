﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.2 
// ------------------------------------------------------------------
// Description:
// This will take all the inventory items it can from any
// object that touches it that has an inventory.
// ------------------------------------------------------------------
// Updates:
// 1.2 - item taken event, handled in inventory instead
// 1.1 - Moved DisplayItems() to its own component class
// ------------------------------------------------------------------
// TODO:
// 
// ============================================================================

[RequireComponent(typeof(Inventory))]
public class CollectionPoint : MonoBehaviour
{
    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private List<ItemData> _acceptedItems;

    // Private Variables ------------------------------------------------------
    private Inventory _inventory;

    // Properties -------------------------------------------------------------
    public Inventory Inventory
    { get { return _inventory; } }

    public ItemData[] AcceptedItems
    { get { return _acceptedItems.ToArray(); } }

    // Unity Functions --------------------------------------------------------

    void Start()
    {
        _inventory = GetComponent<Inventory>();

        //Remove duplicates by re-adding through AddShoppingItems()
        ItemData[] items = _acceptedItems.ToArray();
        _acceptedItems.Clear();
        AddAcceptedItems(items);
    }
    
    void OnTriggerEnter(Collider other)
    {
        Inventory inv = other.gameObject.GetComponent<Inventory>();
        if (inv != null)
        {
            TakeInventory(inv);
        }
    }

    // Public Functions -------------------------------------------------------

    //Add items that are accepted by the basket
    public void AddAcceptedItems(ItemData[] items)
    {
        foreach (ItemData item in items)
        {
            if (!_acceptedItems.Contains(item))
            {
                _acceptedItems.Add(item);
            }
        }
    }

    // Private Functions ------------------------------------------------------

    private void TakeInventory(Inventory inventory)
    {
        if (_inventory.AddItems(inventory.Items.ToArray()))
        {
            inventory.TakeAll();
        }
    }
}
