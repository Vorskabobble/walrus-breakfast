﻿using UnityEngine;
using UnityEngine.Events;

using System.Collections.Generic;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.2 
// ------------------------------------------------------------------
// Description:
// Tracks the progress of an objective and score and generates
// a new objective when the last is completed
// ------------------------------------------------------------------
// Updates:
// 1.2 + new objective event
// 1.1 + ItemsToCollect property
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

public class CollectionObjectiveTracker : MonoBehaviour
{
    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private ScoreData _score;
    [SerializeField]
    private GameTimer _timer;
    [SerializeField]
    private int _itemsPerObjective;
    [SerializeField]
    private CollectionPoint _collectionPoint;
    [SerializeField]
    private CollectionObjective _objective;

    [SerializeField]
    private UnityEvent _objectiveCompleteEvent;
    [SerializeField]
    private UnityEvent _newObjectiveEvent;
    [SerializeField]
    private UnityEvent _objectiveUpdatedEvent;

    // Private Variables ------------------------------------------------------
    private ItemData[] _collectedItems;

    // Properties -------------------------------------------------------------
    public ItemData[] CollectedItems
    { get { return _collectedItems; } }
    public ItemData[] ItemsToCollect
    { get { return _objective.ItemsToCollect; } }

    // Unity Functions --------------------------------------------------------

    void Awake()
    {
        _score._perfectScores = 0;
        _score._collectedItems = 0;
        _score._score = 0;
        _score.TimeBoostedRound = false;

        _collectedItems = new ItemData[_itemsPerObjective];
    }

    // Public Functions -------------------------------------------------------

    public void NewObjective()
    {
        _objective.NewObjective(_collectionPoint.AcceptedItems, _itemsPerObjective);
        _newObjectiveEvent.Invoke();
    }

    // Updates Objective, checking if its complete
    public void UpdateObjective()
    {
        int checkedItemIndex = 0;
        _collectedItems = new ItemData[_itemsPerObjective];

        bool collectedAllItems = true;

        List<ItemData> collectedItems = _collectionPoint.Inventory.Items;
        //take each item that matches an objective item
        foreach (ItemData item in _objective.ItemsToCollect)
        {
            if (collectedItems.Contains(item))
            {
                collectedItems.Remove(item);
                _collectedItems[checkedItemIndex] = item;
                ++checkedItemIndex;
            }
            else
            {
                collectedAllItems = false;
            }
        }

        _objectiveUpdatedEvent.Invoke();

        if (collectedAllItems)
        {
            ObjectiveComplete();
        }
        else
        {
            foreach (ItemData item in _collectedItems)
            {
                if (item != null)
                {
                    _collectionPoint.Inventory.Items.Add(item);
                }
            }
        }
    }

    // Private Functions ------------------------------------------------------

    private void ObjectiveComplete()
    {
        if (_collectedItems.Length != _objective.ItemsToCollect.Length) { return; }

        int collectedItemsCount = 0;
        float multiplier = 1;
        if (_collectionPoint.Inventory.Items.Count == 0)
        {
            ++_score._perfectScores;
            _score._hasPerfectScore = true;
            multiplier = _score._perfectMultiplier;
        }
        else
        {
            _score._hasPerfectScore = false;
            collectedItemsCount += _collectionPoint.Inventory.Items.Count;
        }
        _collectionPoint.Inventory.TakeAll();

        _score._score += _collectedItems.Length * multiplier;
        _score._collectedItems = _collectedItems.Length + collectedItemsCount;

        foreach (ItemData item in _collectedItems)
        {
            _timer.AddTime(item._timeBoost);
        }

        _collectedItems = new ItemData[0];
        _objectiveCompleteEvent.Invoke();
        NewObjective();
    }
}
