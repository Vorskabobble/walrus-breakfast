﻿using UnityEngine;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.0 
// ------------------------------------------------------------------
// Description:
// Creates and stores a list of objects that bneed to be collected 
// ------------------------------------------------------------------
// Updates:
//
// ------------------------------------------------------------------
// TODO:
// 
// ============================================================================

[CreateAssetMenu( fileName = "Collection Objective" )]
public class CollectionObjective : ScriptableObject
{
    // Private Variables ------------------------------------------------------
    private ItemData[] _itemsToCollect;

    // Properties -------------------------------------------------------------
    public ItemData[] ItemsToCollect
    { get { return _itemsToCollect; } }

    // Public Functions -------------------------------------------------------

    //Randomly generate a new objective list from "acceptedItems" of size "shoppingListSize"
    public void NewObjective( ItemData[] acceptedItems, int amountToCollect )
    {
        if(acceptedItems.Length <= 0)
        { return; }

        _itemsToCollect = new ItemData[amountToCollect];

        for( int i = 0; i < amountToCollect; ++i )
        {
            _itemsToCollect[i] = acceptedItems[Random.Range( 0, acceptedItems.Length )];
        }
    }
}
