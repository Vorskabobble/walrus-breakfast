﻿using UnityEngine;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.0
// ------------------------------------------------------------------
// Description:
// Attach to a camera to make it follow and look at the "_target",
// fading any "WallFade" objects in between camera and target.
// ------------------------------------------------------------------
// Updates:
// 
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

public class CameraFollow : MonoBehaviour
{
    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private GameObject _target;
    [SerializeField]
    private float _distance;
    [SerializeField]
    private float _height;

    // Unity Functions --------------------------------------------------------

    void Update()
    {
        if( _target != null )
        {
            UpdatePosition();
            FadeBlockingWalls();
        }
    }

    // Private Functions ------------------------------------------------------

    //Move in relation to the target object
    private void UpdatePosition()
    {
        Vector3 targetPosition = _target.transform.position;
        Vector3 newPosition = new Vector3(targetPosition.x, targetPosition.y + _height, targetPosition.z - _distance);

        transform.position = Vector3.MoveTowards(transform.position, newPosition, 10 * Time.deltaTime);
        transform.LookAt(_target.transform);
    }

    //Check if walls obstruct target and fade them
    private void FadeBlockingWalls()
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        if(Physics.Raycast(ray, out hit, _distance, 1 << 8))
        {
            ObjectFade wall = hit.transform.gameObject.GetComponent<ObjectFade>();
            if (wall != null)
            {
                wall.TempoaryFade();
            }
        }
    }
}
