﻿using UnityEngine;

// ============================================================================
// Copyright ©, Christopher Gough 2015
//
// Author : Christopher Gough
// Editors : Corey Bradford - 1.1, 1.2
// Version : 1.2 
// ------------------------------------------------------------------
// Description:
// Displays a marker around the edge of the screen pointing to an
// object not visible on the screen.
// ------------------------------------------------------------------
// Updates:
// 1.2 + Removes marker when disabled
// 1.2 ~ Fixed marker switching sides when object behind camera
// 1.1 + border rect so marker point can be onscreen before
//       marker disappears
// 1.1 ~ Canvasmarker now public instead of using FindWithTag
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

public class OffscreenMarker : MonoBehaviour
{
    public GameObject screenMarker;
    public Canvas m_canvasMarker;
    public Rect border;

    GameObject m_curMarker;

    public void OnDisable()
    {
        if(m_curMarker != null)
        {
            Destroy(m_curMarker);
        }
    }

    void Start()
    {
        m_curMarker = null;
    }

    void Update()
    {
        Vector3 screenPos = Camera.main.WorldToScreenPoint(transform.position);
        Vector2 canvasSize = m_canvasMarker.GetComponent<RectTransform>().sizeDelta;
        canvasSize *= m_canvasMarker.GetComponent<RectTransform>().localScale.x;

        if (screenPos.z < 0.0f)
        {
            screenPos = -screenPos;
            screenPos.x += Screen.width;
        }

        if (m_curMarker)
        {
            RectTransform markerTrans = m_curMarker.GetComponent<RectTransform>();
            Vector3 rot = Vector3.zero;
            Vector3 pos = Vector3.zero;
            CalculateMarkerOrientation(screenPos, canvasSize, ref rot, ref pos);
            Quaternion markerRot = markerTrans.rotation;
            markerRot.eulerAngles = rot;
            markerTrans.rotation = markerRot;
            markerTrans.position = pos;
        }
        ShowMarker(screenPos, canvasSize);
    }

    void ShowMarker(Vector3 screenPos, Vector2 screenSize)
    {
        if (screenPos.x < 0 + border.x || screenPos.x > screenSize.x - border.width || screenPos.y < 0 + border.y || screenPos.y > screenSize.y - border.height)
        {
            if (!m_curMarker)
            {
                Quaternion rot = Quaternion.identity;
                Vector3 rotVec3 = Vector3.zero;
                Vector3 pos = Vector3.zero;
                CalculateMarkerOrientation(screenPos, screenSize, ref rotVec3, ref pos);
                rot.eulerAngles = rotVec3;
                m_curMarker = Instantiate(screenMarker, pos, rot) as GameObject;
                m_curMarker.GetComponent<RectTransform>().SetParent(m_canvasMarker.transform);
            }
        }
        else if (m_curMarker)
        {
            Destroy(m_curMarker);
            m_curMarker = null;
        }
    }

    void CalculateMarkerOrientation(Vector3 screenPos, Vector2 canvasSize, ref Vector3 rot, ref Vector3 pos)
    {
        Vector3 relativeScreenPos = screenPos;
        relativeScreenPos.x -= canvasSize.x / 2;
        relativeScreenPos.y -= canvasSize.y / 2;

        Vector3 dir = relativeScreenPos.normalized;
        float rotZ = Mathf.Atan(dir.y / dir.x);
        rotZ *= (180 / Mathf.PI);

        rotZ += relativeScreenPos.x < 0 ? 90 : -90;

        RectTransform markerTrans = screenMarker.GetComponent<RectTransform>();
        if (screenPos.x < markerTrans.sizeDelta.x / 2)
        {
            screenPos.x = (markerTrans.sizeDelta.x / 2);
        }
        else if (screenPos.x > canvasSize.x - (markerTrans.sizeDelta.x / 2))
        {
            screenPos.x = canvasSize.x - (markerTrans.sizeDelta.x / 2);
        }

        if (screenPos.y < markerTrans.sizeDelta.y / 2)
        {
            screenPos.y = (markerTrans.sizeDelta.y / 2);
        }
        else if (screenPos.y > canvasSize.y - (markerTrans.sizeDelta.y / 2))
        {
            screenPos.y = canvasSize.y - (markerTrans.sizeDelta.y / 2);
        }

        screenPos.z = 0.0f;
        rot = new Vector3(0, 0, rotZ);
        pos = screenPos;
    }

    void OnDestroy()
    {
        if (m_curMarker)
        {
            Destroy(m_curMarker);
        }
    }
}