﻿using UnityEngine;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.0 
// ------------------------------------------------------------------
// Description:
// 
// ------------------------------------------------------------------
// Updates:
//
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

[RequireComponent(typeof(Inventory))]
public class DisplayInventory : MonoBehaviour 
{
    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private GameObject[] _displayPoints;

    // Private Variables ------------------------------------------------------
    Inventory _inventory;

    // Unity Functions --------------------------------------------------------

    void Start()
    {
        _inventory = GetComponent<Inventory>();
    }

    // Public Functions -------------------------------------------------------

    //Removes all items from display
    public void ClearDisplay()
    {
        foreach(GameObject point in _displayPoints)
        {
            foreach(Transform child in point.transform)
            {
                Destroy( child.gameObject );
            }
        }
    }

    //Removes all items from display then redisplays them
    public void UpdateDisplay()
    {
        ClearDisplay();
        DisplayItems();
    }

    //Displays items with clearing existing items
    public void DisplayItems()
    {
        int index = 0;
        foreach (GameObject point in _displayPoints)
        {
            if (point.transform.childCount == 0)
            {
                if (_inventory.Items.Count > index)
                {
                    GameObject newItem = Instantiate(_inventory.Items[index]._model, point.transform.position, point.transform.rotation) as GameObject;
                    newItem.transform.SetParent(point.transform);
                }
            }
            ++index;
        }
    }
}
