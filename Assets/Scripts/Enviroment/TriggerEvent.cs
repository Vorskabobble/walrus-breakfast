﻿using UnityEngine;
using UnityEngine.Events;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.0
// ------------------------------------------------------------------
// Description:
// Helper script for basic trigger calback events
// ------------------------------------------------------------------
// Updates:
// 
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

public class TriggerEvent : MonoBehaviour
{
    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private string _collideTag;

    [SerializeField]
    private UnityEvent _triggerEnterEvent;
    [SerializeField]
    private UnityEvent _triggerExitEvent;
    [SerializeField]
    private UnityEvent _triggerStayEvent;

    // Unity Functions --------------------------------------------------------

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(_collideTag))
        {
            _triggerEnterEvent.Invoke();
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if(other.CompareTag(_collideTag))
        {
            _triggerExitEvent.Invoke();
        }
    }

    public void OnTriggerStay(Collider other)
    {
        if(other.CompareTag(_collideTag))
        {
            _triggerStayEvent.Invoke();
        }
    }
}
