﻿using UnityEngine;
using System.Collections;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.0 
// ------------------------------------------------------------------
// Description:
// Spawns an item at its position, if the item is taken it will
// spawn another item after a certain time. 
// ------------------------------------------------------------------
// Updates:
//
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

public class ItemSpawner : MonoBehaviour
{
    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private float _spawnTime;
    [SerializeField]
    private ItemData _spawnable;
    
    // Properties -------------------------------------------------------------
    public ItemData Spawnable
    { get { return _spawnable; } set { _spawnable = value; } }

    // Unity Functions --------------------------------------------------------

    void Start()
    {
        CreateItem();
    }

    public void OnTransformChildrenChanged()
    {
        if( transform.childCount == 0 )
        {
            StartCoroutine( SpawnItem( _spawnTime ) );
        }
    }

    // Private Functions ------------------------------------------------------

    private IEnumerator SpawnItem( float timeBeforeSpawn )
    {
        float currentTime = 0.0f;
        while( currentTime < timeBeforeSpawn )
        {
            currentTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        CreateItem();
    }

    private void CreateItem()
    {
        GameObject spawnedObject = Instantiate( _spawnable._scenePrefab, transform.position, transform.rotation ) as GameObject;
        spawnedObject.transform.SetParent( transform );
    }
}
