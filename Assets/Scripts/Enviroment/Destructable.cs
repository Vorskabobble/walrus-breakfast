﻿using UnityEngine;
using UnityEngine.Events;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.1 
// ------------------------------------------------------------------
// Description:
// Makes object fall apart by enabling physics on child
// objects. Has to be hit by an object travelling faster
// then "_impactSpeed".
// ------------------------------------------------------------------
// Updates:
// 1.1 ~ Changed "OnCollisionEnter" to "OnTriggerEnter"
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

public class Destructable : MonoBehaviour
{
    // Editor Variables -------------------------------------------------------
    [SerializeField]
    float _impactSpeed;
    [SerializeField]
    private UnityEvent _onDestroyedEvent;

    // Unity Functions --------------------------------------------------------

    void Start()
    {
        foreach (Transform child in gameObject.transform)
        {
            Rigidbody childRB = child.gameObject.GetComponent<Rigidbody>();
            if (childRB != null)
            {
                childRB.constraints = RigidbodyConstraints.FreezeAll;
            }
        }
    }

    public void OnTriggerEnter(Collider Collider)
    {
        if(Collider.gameObject.CompareTag("Player"))
        {
            Rigidbody playerRB = Collider.gameObject.GetComponent<Rigidbody>();
            if (playerRB != null)
            {
                if(playerRB.velocity.magnitude > _impactSpeed)
                {
                    FallApart();
                }
            }
        }
    }

    // Private Functions ------------------------------------------------------

    //Enables physics on child objects
    private void FallApart()
    {
        foreach(Transform child in gameObject.transform)
        {
            Rigidbody childRB = child.gameObject.GetComponent<Rigidbody>();
            if(childRB != null)
            {
                childRB.constraints = RigidbodyConstraints.None;
            }
        }
        _onDestroyedEvent.Invoke();
    }
}
