﻿using UnityEngine;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.2 
// ------------------------------------------------------------------
// Description:
// Fades an object to 0 alpha if its between the camera and the
// player, will unfade if no longer in the way.
// ------------------------------------------------------------------
// Updates:
// 1.2 + permenant fade
// 1.2 ~ refactored to ObjectFade
// 1.1 ~ Fixed object not un-fading
// ------------------------------------------------------------------
// TODO:
// - Make into a general fade script
// ============================================================================

delegate void HandleFading();

public class ObjectFade : MonoBehaviour
{
    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private float _fadeTime;

    // Private Variables ------------------------------------------------------
    bool _isFading;
    bool _stayFaded;

    float _alphaValue;
    float _startAlpha;

    float _fadePerSecond;

    Material _material;
    HandleFading _fadingDelegate;

    // Unity Functions --------------------------------------------------------

    void Start()
    {
        _material = GetComponent<Renderer>().material;
        _startAlpha = _material.color.a;
        _alphaValue = _startAlpha;

        _fadePerSecond = _startAlpha / _fadeTime;
    }

    void Update()
    {
        if (_fadingDelegate != null)
        {
            _fadingDelegate();
        }
    }

    // Public Functions -------------------------------------------------------

    //Fades object every frame its called
    public void TempoaryFade()
    {
        _isFading = true;
        UpdateAlpha(-_fadePerSecond * Time.deltaTime);
        _fadingDelegate = Unfade;
    }

    //Fades the object and keeps it faded
    public void PermenentFade()
    {
        _fadingDelegate = Fade;
    }

    //Stop fading if fading was started with "PermenentFade()"
    public void StopFading()
    {
        _fadingDelegate = Unfade;
    }

    // Private Functions ------------------------------------------------------

    //Add "change" the the materials alpha
    private void UpdateAlpha(float change)
    {
        Color matColour = _material.color;

        _alphaValue += change;
        _alphaValue = Mathf.Clamp(_alphaValue, 0, _startAlpha);
        matColour.a = _alphaValue;

        _material.color = matColour;
    }

    private void Fade()
    {
        if(_alphaValue > 0.0f)
        {
            UpdateAlpha(-_fadePerSecond * Time.deltaTime);
        }
    }

    private void Unfade()
    {
        if( !_isFading && _alphaValue < _startAlpha )
        {
            UpdateAlpha( _fadePerSecond * Time.deltaTime );
        }
        _isFading = false;
    }
}
