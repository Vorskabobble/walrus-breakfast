﻿using UnityEngine;
using UnityEngine.Events;

using System.Collections.Generic;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.3
// ------------------------------------------------------------------
// Description:
// This stores "itemData" objects in a basic inventory 
// system.
// ------------------------------------------------------------------
// Updates:
// 1.3 + Item added Event
// 1.3 + Item removed Event
// 1.2 + Added null checks so null items aren't added
// 1.1 + "AddItems()" to add more then one item at a time
// 1.1 + Setting "_maxItems" to 0 holds unlimited items
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

public class Inventory : MonoBehaviour
{
    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private int _maxItems;
    [SerializeField]
    private UnityEvent _itemAddedEvent;
    [SerializeField]
    private UnityEvent _itemRemovedEvent;

    // Private Variables ------------------------------------------------------
    private List<ItemData> _items;

    // Properties -------------------------------------------------------------
    public List<ItemData> Items
    { get { return _items; } }

    public int MaxItems
    { get { return _maxItems; } }

    // Unity Functions --------------------------------------------------------

    void Awake()
    {
        _items = new List<ItemData>();
    }

    // Public Functions -------------------------------------------------------

    //Add an item to inventory, returns true if successful
    public bool AddItem( ItemData item )
    {
        if( item == null )
        { return false; }

        if( CanAddItems( 1 ) )
        {
            _items.Add( item );
            _itemAddedEvent.Invoke();
            return true;
        }
        return false;
    }

    //Add multiple items to inventory, returns true if all items can be added
    public bool AddItems( ItemData[] items )
    {
        if( CanAddItems( items.Length ) )
        {
            _items.AddRange( items );
            _items.RemoveAll( item => item == null );
            _itemAddedEvent.Invoke();
            return true;
        }
        return false;
    }

    //Removes and item from inventory, returns true if successful
    public bool TakeItem( ItemData item )
    {
        if( _items.Contains( item ) )
        {
            _items.Remove( item );
            _itemRemovedEvent.Invoke();
            return true;
        }
        return false;
    }

    //Clears all items from the inventory
    public void TakeAll()
    {
        _items.Clear();
        _itemRemovedEvent.Invoke();
    }

    // Private Functions ------------------------------------------------------

    private bool CanAddItems( int count )
    {
        if( _maxItems == 0 || _items.Count + count <= _maxItems )
        { return true; }
        return false;
    }
}