﻿using UnityEngine;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.1
// ------------------------------------------------------------------
// Description:
// Stores data for each unique item 
// ------------------------------------------------------------------
// Updates:
// 1.1 + "_model" to display item without full prefab
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

[CreateAssetMenu(fileName = "Item", menuName = "Item Data")]
public class ItemData : ScriptableObject
{
    // Public Variables -------------------------------------------------------
    public float _timeBoost;

    public GameObject _scenePrefab;
    public GameObject _model;
    public Sprite _icon;
}
