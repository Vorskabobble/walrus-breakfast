﻿using UnityEngine;
using UnityEngine.Events;

using System.Collections;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.1 
// ------------------------------------------------------------------
// Description:
// This lets a gameobject be "collected" by passing its item data to
// the collector and destroying itself
// ------------------------------------------------------------------
// Updates:
// 1.1 + Plays animations instead of particle when collected
// ------------------------------------------------------------------
// TODO:
// + Animation support
// ============================================================================


public class Collectable : MonoBehaviour
{
    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private ItemData _itemData;
    [SerializeField]
    private AudioSource _collectionSound;

    // Properties -------------------------------------------------------------
    public ItemData Data
    { get { return _itemData; } set { _itemData = value; } }

    // Public Functions -------------------------------------------------------

    //Plays collection anim and removes object from world
    public void Collect()
    {
        Animator animator = GetComponent<Animator>();
        if (animator != null)
        {
            animator.SetBool("Collect", true);
        }
        StartCoroutine(DelayDestroy());
    }

    public IEnumerator DelayDestroy()
    {
        Renderer renderComponent= GetComponent<Renderer>();
        renderComponent.enabled = false;
        
        foreach(Collider collideComponent in GetComponents<Collider>())
        {
            collideComponent.enabled = false;
        }

        if (_collectionSound != null)
        {
            _collectionSound.Play();
            while (_collectionSound.isPlaying)
            {
                yield return new WaitForEndOfFrame();
            }
            Destroy(this.gameObject);
        }
        Destroy(this.gameObject);
    }
}
