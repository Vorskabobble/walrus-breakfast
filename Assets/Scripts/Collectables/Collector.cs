﻿using UnityEngine;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.1
// ------------------------------------------------------------------
// Description:
// This lets a gameobject "collect" any collectables it  
// collides with
// ------------------------------------------------------------------
// Updates:
// 1.1 - item display, solely focuses on collecting an item
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

[RequireComponent(typeof(Inventory))]
public class Collector : MonoBehaviour
{
    // Private Variables ------------------------------------------------------
    private Inventory _inventory;

    // Unity Functions --------------------------------------------------------  
      
    public void Start()
    {
        _inventory = GetComponent<Inventory>();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Collectable"))
        {
            Collectable touchedItem = other.GetComponent<Collectable>();
            if(_inventory.AddItem(touchedItem.Data))
            {
                touchedItem.Collect();
            }
        }
    }
}
