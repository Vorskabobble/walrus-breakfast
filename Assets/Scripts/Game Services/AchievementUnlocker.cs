﻿using UnityEngine;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.0 
// ------------------------------------------------------------------
// Description:
// 
// ------------------------------------------------------------------
// Updates:
//
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

public class AchievementUnlocker : MonoBehaviour
{
    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private SocialServices _services;
    [SerializeField]
    private ScoreData _playerScore;

    // Private Variables ------------------------------------------------------
    private int _itemsCounted;

    // Public Functions -------------------------------------------------------

    public void UpdateRoundEndAchievements()
    {
        if( !Social.localUser.authenticated || _playerScore._timeBoostedRound )
        { return; }

        _services.UnlockAchievement( GPGSIds.achievement_the_beggining );
        _services.UpdateAchievement( GPGSIds.achievement_on_a_roll, 1 );
        _services.UpdateAchievement( GPGSIds.achievement_walrus_roller, 1 );
    }

    public void UpdateItemCollectedAchievements()
    {
        if(!Social.localUser.authenticated)
        { return; }

        int newItems = _playerScore._collectedItems - _itemsCounted;

        _services.UpdateAchievement( GPGSIds.achievement_collector, newItems );
        _services.UpdateAchievement( GPGSIds.achievement_hoarder, newItems );

        _itemsCounted = _playerScore._collectedItems;
    }
}
