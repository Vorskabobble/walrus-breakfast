﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Advertisements;

// ============================================================================
// Copyright ©, Corey Bradford 2015
//
// Author : Corey Bradford
// Version : 1.0 
// ------------------------------------------------------------------
// Description:
// Plays adverts every 5 rounds since the last ad, this includes
// reward ads. Can also play reward adverts and calls rewardAdPlayed 
// event.
// ------------------------------------------------------------------
// Updates:
//
// ------------------------------------------------------------------
// TODO:
//
// ============================================================================

public class Adverts : MonoBehaviour
{
    // Editor Variables -------------------------------------------------------
    [SerializeField]
    private int _roundsBetweenAds;

    [SerializeField]
    private UnityEvent _rewardAdPlayed;

    // Public Functions -------------------------------------------------------

    public void RoundEnded()
    {
        PlayerPrefs.SetInt( "rounds", PlayerPrefs.GetInt( "rounds" ) + 1 );
    }

    public void ShowNormalAds()
    {
        if( PlayerPrefs.GetInt( "rounds" ) >= _roundsBetweenAds )
        {
            if( Advertisement.IsReady( "video" ) )
            {
                Advertisement.Show( "video" );
                PlayerPrefs.SetInt( "rounds", 0 );
            }
        }
    }

    public void ShowRewardAd()
    {
        if( Advertisement.IsReady( "rewardedVideo" ) )
        {
            ShowOptions options = new ShowOptions { resultCallback = AdShowResult };
            Advertisement.Show( "rewardedVideo", options );
            PlayerPrefs.SetInt( "rounds", 0 );
        }
    }

    void AdShowResult( ShowResult result )
    {
        switch( result )
        {
            case ShowResult.Finished:
                _rewardAdPlayed.Invoke();
                break;
            case ShowResult.Skipped:
            case ShowResult.Failed:
            default:
                break;
        }
    }
}
